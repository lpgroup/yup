const { validate } = require("./utils");

// TODO: Can probably remove options.schema
function validateRequest(options = {}) {
  return async (context) => {
    if (context.method === "create" || context.method === "update") {
      context.data = await validate(
        context.data,
        (options.schema && options.schema.request) || context.service.options.schema.request,
      );
    } else if (context.method === "patch") {
      // TODO check that only keys from requestSchema
      // are in the request json.
      // eslint-disable-next-line
    }
    return context;
  };
}

// eslint-disable-next-line no-unused-vars
function validateDatabase(options = {}) {
  return async (context) => {
    context.data = await validate(context.data, context.service.options.schema.db);
    return context;
  };
}

module.exports = { validateRequest, validateDatabase };
