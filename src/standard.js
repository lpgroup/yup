const yup = require("yup");
const { v4: uuidv4 } = require("uuid");
const nano = require("nanoid");
const {
  object,
  arrayObject,
  string,
  timestamp,
  fromDateTime,
  toDateTime,
} = require("./general");

const LABEL_TEXT_MIN_SIZE = 1;
const LABEL_TEXT_MAX_SIZE = 85;
const SHORT_TEXT_MAX_SIZE = 250;
const MEDIUM_TEXT_MAX_SIZE = 600;
const LONG_TEXT_MAX_SIZE = 50000;
const URL_MAX_SIZE = 255;
const IMAGE_ALT_MAX_SIZE = 125;

// TODO: rename id, nanoid and uuid maybe autoUuid
function id() {
  return yup.lazy(() => yup.string().default(uuidv4()));
}

function nanoid() {
  return yup.lazy(() => yup.string().default(nano.nanoid()));
}

function uuid() {
  return string();
}

function alias() {
  return string().min(LABEL_TEXT_MIN_SIZE).max(LABEL_TEXT_MAX_SIZE).lowercase();
}

function title() {
  return string().min(LABEL_TEXT_MIN_SIZE).max(LABEL_TEXT_MAX_SIZE);
}

function labelText() {
  return string().max(LABEL_TEXT_MAX_SIZE);
}

function shortText() {
  return string().max(SHORT_TEXT_MAX_SIZE);
}

function mediumText() {
  return string().max(MEDIUM_TEXT_MAX_SIZE);
}

function longText() {
  return string().max(LONG_TEXT_MAX_SIZE);
}

function description() {
  return object({
    short: shortText().defaultNull(),
    text: longText().defaultNull(),
  });
}

function imageUri() {
  return yup.string();
}

function pdfUri() {
  return yup.string().matches(/^data:application\/pdf;base64/);
}

function referenceNumber() {
  return yup.number().integer().min(0);
}

// This can hold both positive and negative numbers.
function amount() {
  return yup.number().integer();
}

function positiveAmount() {
  return amount().min(0);
}

function currency() {
  return yup.string().oneOf(["sek"]);
}

function vatId() {
  return yup.string().oneOf(["vat-06", "vat-12", "vat-25"]);
}

function bankGiro() {
  return labelText().defaultNull();
}

function postGiro() {
  return labelText().defaultNull();
}

function email() {
  return string().lowercase().min(5).max(254); // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address/44317754
}

// TODO
// eslint-disable-next-line max-len
// const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
// yup.string().matches(phoneRegExp, "Ange ett giltigt telefonnummer"),
function phone() {
  return string().min(5).max(LABEL_TEXT_MAX_SIZE).defaultNull();
}

function tags() {
  return string().min(0).max(MEDIUM_TEXT_MAX_SIZE).lowercase();
}

function url() {
  return string().max(URL_MAX_SIZE);
}

function images() {
  return arrayObject({
    _id: id(),
    src: url().required(),
    alt: string().max(IMAGE_ALT_MAX_SIZE).required(),
  });
}

function notRequiredImage() {
  return object({
    _id: id(),
    src: url().defaultNull(),
    alt: string().max(IMAGE_ALT_MAX_SIZE).defaultNull(),
  });
}

function socialSecurityNumber() {
  return labelText().defaultNull();
}

function organisationNumber() {
  return labelText().defaultNull();
}

function address() {
  return object({
    street: labelText().defaultNull(),
    city: labelText().defaultNull(),
    state: labelText().defaultNull(),
    zipCode: labelText().defaultNull(),
    country: labelText().defaultNull(),
  });
}

function emailContact() {
  return object({
    email: email().defaultNull(),
    lastName: labelText().defaultNull(),
    firstName: labelText().defaultNull(),
  });
}

function location() {
  return object({
    latitude: yup.number().min(-80).max(80).defaultNull(),
    longitude: yup.number().min(-180).max(180).defaultNull(),
  });
}

function publish() {
  return object({
    active: yup.boolean().default(false),
    from: fromDateTime().defaultNull(),
    to: toDateTime("from").defaultNull(),
  });
}

function changed() {
  return object({
    by: uuid().required(),
    at: timestamp().required(),
  });
}

function owner() {
  return object({
    organisation: object({
      _id: uuid().required(),
    }).required(),
  });
}

function meta() {
  return yup.lazy((value) => {
    switch (typeof value) {
      case "number":
        return yup.number();
      case "string":
        return yup.string();
      case "object":
        return yup.object();
      // This set the default value to {}
      case "undefined":
        return yup.object().default({});
      default:
        return yup.mixed();
    }
  });
}

module.exports = {
  id,
  nanoid,
  uuid,
  alias,
  title,
  labelText,
  shortText,
  mediumText,
  longText,
  description,
  imageUri,
  pdfUri,
  referenceNumber,
  amount,
  positiveAmount,
  currency,
  vatId,
  bankGiro,
  postGiro,
  email,
  phone,
  tags,
  url,
  images,
  notRequiredImage,
  socialSecurityNumber,
  organisationNumber,
  address,
  emailContact,
  location,
  publish,
  changed,
  owner,
  meta,
};
