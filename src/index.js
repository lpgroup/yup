const yup = require("yup");
const YupError = require("./YupError");
const utils = require("./utils");
const general = require("./general");
const standard = require("./standard");
const hooks = require("./hooks");

// eslint-disable-next-line func-names
yup.addMethod(yup.mixed, "defaultNull", function () {
  return this.nullable().default(null);
});

module.exports = {
  lazy: yup.lazy,
  boolean: yup.boolean,
  number: yup.number,
  mixed: yup.mixed,
  YupError,
  ...utils,
  ...general,
  ...standard,
  ...hooks,
};
