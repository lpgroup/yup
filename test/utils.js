const yup = require("../src/index");

function validate(data, schema) {
  const valid = yup.validate(data, yup.object(schema));
  return expect(valid);
}

module.exports = { validate };
