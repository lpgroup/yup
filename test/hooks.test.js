const {
  buildValidationSchema,
  validateRequest,
  validateDatabase,
  string,
} = require("../src/index");

function buildContext(data, requestSchema, databaseSchema) {
  return {
    data,
    method: "create",
    service: {
      options: {
        schema: buildValidationSchema(requestSchema, databaseSchema),
      },
    },
  };
}

describe("Hooks", () => {
  describe("validateRequest", () => {
    const schema = { key: string().required() };
    const validateRequestHook = validateRequest();

    test("valid", async () => {
      const context = buildContext({ key: "valid " }, schema, schema);
      expect(validateRequestHook(context)).resolves.toEqual(
        expect.objectContaining({
          data: { key: "valid" },
        }),
      );
    });

    test("invalid", async () => {
      const context = buildContext({}, schema, schema);
      expect(validateRequestHook(context)).rejects.toThrow("key is a required field");
    });
  });

  describe("validateDatabase", () => {
    const schema = { key: string().required() };
    const validateDatabaseHook = validateDatabase();

    test("valid", async () => {
      const context = buildContext({ key: "valid " }, schema, schema);
      expect(validateDatabaseHook(context)).resolves.toEqual(
        expect.objectContaining({
          data: { key: "valid" },
        }),
      );
    });

    test("invalid", async () => {
      const context = buildContext({}, schema, schema);
      expect(validateDatabaseHook(context)).rejects.toThrow("key is a required field");
    });
  });
});
