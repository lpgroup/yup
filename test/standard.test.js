const { validate } = require("./utils");
const yup = require("../src/index");

describe("Standard", () => {
  test("id valid", async () => {
    const schema = { key: yup.id() };
    validate({ key: 0 }, schema).resolves.toStrictEqual({ key: "0" });
    validate({ key: 13 }, schema).resolves.toStrictEqual({ key: "13" });
    validate({ key: "37" }, schema).resolves.toStrictEqual({ key: "37" });
    validate({ key: "" }, schema).resolves.toStrictEqual({ key: "" });
    validate({}, schema).resolves.not.toBe("");
    validate({ key: { key2: "value" } }, schema, { key: "[object Object]" });
    validate({ key: ["13", "37"] }, schema, { key: "13,37" });
  });

  test("id invalid", async () => {
    const schema = { key: yup.id() };
    const msg = "key must be a `string` type, but the final value was: `null`.\n "
      + "If \"null\" is intended as an empty value be sure to mark the schema as `.nullable()`";
    validate({ key: null }, schema).rejects.toThrow(msg);
  });
});
